package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media;

import java.io.BufferedReader;
import java.io.IOException;

/**
 *Interface permettant la lecture d'un flux de caractère
 * @param <T>
 */
public interface ResumableImageFrameMedia<T> extends ImageFrameMedia<T> {

    /**
     * Prototype afin de lire un flux de caractère puis le mettre en tampon.
     * @param resume
     * @return
     * @throws IOException
     */
    BufferedReader getEncodedImageReader(boolean resume) throws IOException;
}
